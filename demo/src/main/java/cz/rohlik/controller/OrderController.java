package cz.rohlik.controller;

import cz.rohlik.dto.CreateOrderRequest;
import cz.rohlik.dto.OrderDto;
import cz.rohlik.dto.ProductDto;
import cz.rohlik.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1")
public class OrderController {
    @Autowired
    OrderService orderService;

    @GetMapping("/order")
    List<OrderDto> findAll(){
       return orderService.findAll();
    }

    @PostMapping("/order")
    List<ProductDto> create(@RequestBody CreateOrderRequest request){
        return orderService.create(request.getProducts());
    }

    @PostMapping("/order/{orderId}/cancel")
    OrderDto cancel(@PathVariable Long orderId){
        return orderService.cancelOrder(orderId);
    }

    @PostMapping("/order/{orderId}/payed")
    OrderDto payed(@PathVariable Long orderId){
        return orderService.fulfill(orderId);
    }

}
