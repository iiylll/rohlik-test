package cz.rohlik.controller;

import cz.rohlik.dto.ChangeProductRequest;
import cz.rohlik.dto.CreateProductRequest;
import cz.rohlik.dto.ProductDto;
import cz.rohlik.model.Product;
import cz.rohlik.service.ProductService;
import cz.rohlik.service.WarehouseShelfService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1")
public class ProductController {
    @Autowired
    ProductService productService;

    @Autowired
    WarehouseShelfService warehouseShelfService;

    @GetMapping("/product")
    @Transactional
    public List<ProductDto> findAll(){
        return warehouseShelfService.findAll();

    }

    @PostMapping("/product")
    @Transactional
    public ProductDto create(@RequestBody CreateProductRequest request){
        Product product = productService.create(request.getName(), request.getPrice());
        return warehouseShelfService.insertProduct(product, request.getQuantity());
    }

    @PutMapping("/product/{productId}")
    @Transactional
    public ProductDto update(@PathVariable Long productId, @RequestBody ChangeProductRequest request){
        productService.update(productId, request.getName(), request.getPrice());
        return warehouseShelfService.updateQuantity(productId, request.getQuantity());
    }

    @DeleteMapping("/product/{productId}")
    public void delete(@PathVariable Long productId){
        productService.delete(productId);
    }
}
