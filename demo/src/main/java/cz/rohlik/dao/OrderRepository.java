package cz.rohlik.dao;

import cz.rohlik.model.Order;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.util.List;

@Repository
public interface OrderRepository extends CrudRepository<Order, Long>  {
    List<Order> findAll();
    void removeByIdAndOrderStatus(Long id, Order.OrderStatus orderStatus);
    @Modifying
    void removeAllByValidUntilBefore(Date date);
}
