package cz.rohlik.dao;

import cz.rohlik.model.Product;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends CrudRepository<Product, Long> {
    int findQuantityById(Long id);

    @Override
    List<Product> findAll();

    List<Product> findAllByName(String name);
}
