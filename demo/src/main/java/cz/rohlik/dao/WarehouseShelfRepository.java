package cz.rohlik.dao;

import cz.rohlik.model.WarehouseShelf;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WarehouseShelfRepository extends CrudRepository<WarehouseShelf, Long> {
    List<WarehouseShelf> findAll();
    List<WarehouseShelf> findAllByProductIdIn(List<Long> productIds);
    WarehouseShelf findByProductId(Long productId);
}
