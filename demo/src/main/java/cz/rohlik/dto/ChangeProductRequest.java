package cz.rohlik.dto;

public class ChangeProductRequest extends ProductRequest {
    public ChangeProductRequest(String name, double price, int quantity) {
        super(name, price, quantity);
    }
}
