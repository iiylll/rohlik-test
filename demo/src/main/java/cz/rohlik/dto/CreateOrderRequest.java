package cz.rohlik.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class CreateOrderRequest {
    public List<ProductDto> getProducts() {
        return products;
    }

    @JsonProperty("products")
    private List<ProductDto> products;

}
