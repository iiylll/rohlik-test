package cz.rohlik.dto;

public class CreateProductRequest extends ProductRequest {
    public CreateProductRequest(String name, double price, int quantity) {
        super(name, price, quantity);
    }
}
