package cz.rohlik.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import cz.rohlik.model.Order;

public class OrderDto {
    @JsonProperty("id")
    private Long id;

    @JsonProperty("status")
    private Order.OrderStatus status;

    public OrderDto(Long id, Order.OrderStatus status){
        this.id = id;
        this.status = status;
    }
}
