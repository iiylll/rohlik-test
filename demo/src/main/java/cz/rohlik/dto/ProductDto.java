package cz.rohlik.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import cz.rohlik.model.Product;

public class ProductDto {
    @JsonProperty("id")
    private Long id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("price")
    private double price;

    @JsonProperty("quantity")
    private int quantity;

    private ProductDto(ProductDtoBuilder builder){
        this.id = builder.id;
        this.name = builder.name;
        this.price = builder.price;
        this.quantity = builder.quantity;
    }

    public ProductDto(){

    }

    public Product copyProduct(){
        return new Product(this.id, this.getName(), this.getPrice());
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public int getQuantity() {
        return quantity;
    }

    public static class ProductDtoBuilder{
        private Long id;
        private String name;
        private double price;
        private int quantity;


        public ProductDtoBuilder product(Product product){
            this.id = product.getId();
            this.name = product.getName();
            this.price = product.getPrice();
            return this;
        }

        public ProductDtoBuilder id(Long id) {
            this.id = id;
            return this;
        }

        public ProductDtoBuilder name(String name) {
            this.name = name;
            return this;
        }

        public ProductDtoBuilder price(double price) {
            this.price = price;
            return this;
        }

        public ProductDtoBuilder quantity(int quantity) {
            this.quantity = quantity;
            return this;
        }
        public ProductDto build(){
            return new ProductDto(this);
        }
    }
}
