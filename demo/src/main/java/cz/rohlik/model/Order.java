package cz.rohlik.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity(name="orders")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column
    protected Date validUntil;

    public enum OrderStatus {
        PENDING, CANCELED, PAYED
    }

    @Enumerated(EnumType.STRING)
    protected OrderStatus orderStatus;

    @OneToMany(fetch = FetchType.EAGER, mappedBy="order", cascade = {CascadeType.ALL})
    List<ProductInOrder> productsInOrder = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public List<ProductInOrder> getProductsInOrder() {
        return productsInOrder;
    }

    public OrderStatus getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
    }

    public Order(OrderStatus orderStatus, List<ProductInOrder> productsInOrder) {
        this.orderStatus = orderStatus;
        this.productsInOrder = productsInOrder;
    }

    public Order(Date validUntil, OrderStatus orderStatus, List<ProductInOrder> productsInOrder){
        this(orderStatus, productsInOrder);
        this.validUntil = validUntil;
    }

    public Order(){

    }
}
