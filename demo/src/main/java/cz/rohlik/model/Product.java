package cz.rohlik.model;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

@Entity(name="products")
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(nullable = false, unique = true)
    private String name;

    @Column(nullable = false)
    private double price;

    @OneToMany(fetch = FetchType.EAGER, mappedBy="product", cascade = {CascadeType.ALL})
    Set<ProductInOrder> productsInOrder;

    @OneToOne(fetch = FetchType.LAZY, mappedBy="product", cascade = {CascadeType.ALL})
    WarehouseShelf warehouseShelf;

    public Long getId() {
        return id;
    }

    public String getName(){
        return this.name;
    }

    public double getPrice(){
        return this.price;
    }

    public void setName(String name){
        this.name = name;
    }

    public void setPrice(double price){
        this.price = price;
    }

    public Product(Long id){
        this.id = id;
    }

    public Product(String name){
        this.name = name;
    }

    public Product(String name, double price){
        this(name);
        this.price = price;
    }

    public Product(Long id, String name, double price){
        this(name, price);
        this.id = id;
    }

    public Product(){

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return id.equals(product.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
