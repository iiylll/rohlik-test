package cz.rohlik.model;

import cz.rohlik.dto.ProductDto;

import javax.persistence.*;

@Entity
public class ProductInOrder {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    Product product;

    @ManyToOne(fetch = FetchType.LAZY)
    Order order;

    @Column(nullable = false)
    int quantity;

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public ProductInOrder(Product product, int quantity) {
        this.product = product;
        this.quantity = quantity;
    }

    public ProductInOrder(ProductDto dto){
        Product product = new Product(dto.getId(), dto.getName(), dto.getPrice());
        this.product = product;
        this.quantity = dto.getQuantity();
    }

    public ProductInOrder(){

    }


}
