package cz.rohlik.model;

import javax.persistence.*;

@Entity
public class WarehouseShelf {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;

    @OneToOne()
    Product product;

    @Column
    int quantity;

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getQuantity() {
        return quantity;
    }

    public void removeQuantity(int quantity){
        this.quantity -= quantity;
    }

    public void addQuantity(int quantity){
        this.quantity += quantity;
    }
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public WarehouseShelf(Product product, int quantity) {
        this.product = product;
        this.quantity = quantity;
    }

    public WarehouseShelf(){

    }
}
