package cz.rohlik.service;


import cz.rohlik.dao.OrderRepository;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;


@Component
public class OrderCleanerService {
    private final OrderRepository orderRepository;

    public OrderCleanerService(final OrderRepository orderRepository) {
        this.orderRepository = orderRepository;

    }

    @Scheduled(fixedRate = 60000)
    @Transactional
    public void create() {
        orderRepository.removeAllByValidUntilBefore(new java.sql.Date(new Date().getTime()));
    }
}