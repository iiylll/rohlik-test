package cz.rohlik.service;

import cz.rohlik.dto.OrderDto;
import cz.rohlik.dto.ProductDto;

import java.util.List;

public interface OrderService {
    List<OrderDto> findAll();
    List<ProductDto> create(List<ProductDto> productsInOrder);
    OrderDto cancelOrder(Long id);
    OrderDto fulfill (Long id);
}
