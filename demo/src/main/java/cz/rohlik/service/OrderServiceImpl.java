package cz.rohlik.service;

import cz.rohlik.dao.OrderRepository;
import cz.rohlik.dto.OrderDto;
import cz.rohlik.dto.ProductDto;
import cz.rohlik.model.Order;
import cz.rohlik.model.ProductInOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.apache.commons.lang.time.DateUtils;
import javax.persistence.EntityNotFoundException;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    OrderRepository orderRepository;


    @Autowired
    WarehouseShelfService warehouseShelfService;

    @Override
    public List<OrderDto> findAll() {
        return orderRepository.findAll().stream().map(x -> new OrderDto(x.getId(), x.getOrderStatus())).collect(Collectors.toList());
    }

    @Override
    @Transactional(isolation = Isolation.REPEATABLE_READ)
    public List<ProductDto> create(List<ProductDto> productsInOrder) {
        List<ProductDto> missingProducts = warehouseShelfService.getMissingProducts(productsInOrder);
        if (missingProducts.isEmpty()){
            warehouseShelfService.removeProductsFromWarehouse(productsInOrder);
            Order order = new Order(halfAnHourFromNow(), Order.OrderStatus.PENDING, missingProducts.stream().map((ProductInOrder::new)).collect(Collectors.toList()));
            orderRepository.save(order);
        }
        return missingProducts;
    }

    public OrderDto cancelOrder(Long id){
        Order o = orderRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        o.setOrderStatus(Order.OrderStatus.CANCELED);
        warehouseShelfService.returnProductsToWarehouse(o.getProductsInOrder());
        o = orderRepository.save(o);
        return new OrderDto(o.getId(), o.getOrderStatus());
    }

    @Override
    public OrderDto fulfill(Long id) {
        Order o = orderRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        o.setOrderStatus(Order.OrderStatus.PAYED);
        o = orderRepository.save(o);
        return new OrderDto(o.getId(), o.getOrderStatus());
    }

    private Date halfAnHourFromNow(){
        Date date = new Date();
        DateUtils.addMinutes(date, 30);
        return date;
    }

}
