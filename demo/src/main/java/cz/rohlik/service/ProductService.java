package cz.rohlik.service;

import cz.rohlik.model.Product;

public interface ProductService {

    Product create(String name, double price);

    Product update(Long id, String newName, double newPrice);

    void delete(Long id);

}
