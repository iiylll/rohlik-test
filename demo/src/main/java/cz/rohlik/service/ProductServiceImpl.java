package cz.rohlik.service;

import cz.rohlik.dao.ProductRepository;
import cz.rohlik.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;

@Service
public class ProductServiceImpl implements ProductService{

    @Autowired
    ProductRepository productRepository;

    @Override
    public Product create(String name, double price) {
        validateName(name);
        validatePrice(price);
        Product product = new Product(name, price);
        return productRepository.save(product);
    }

    @Override
    public Product update(Long id, String newName, double newPrice) {
        validateName(newName);
        validatePrice(newPrice);
        Product foundProduct = productRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        foundProduct.setName(newName);
        foundProduct.setPrice(newPrice);
        return productRepository.save(foundProduct);
    }

    @Override
    public void delete(Long productId) {
        productRepository.deleteById(productId);
    }

    private void validatePrice(double price){
        if(price < 0){
            throw new IllegalStateException();
        }
    }

    private void validateName(String name){
        if (name.isEmpty()){
            throw new IllegalStateException();
        }
    }
}
