package cz.rohlik.service;

import cz.rohlik.dto.ProductDto;
import cz.rohlik.model.Product;
import cz.rohlik.model.ProductInOrder;

import java.util.List;

public interface WarehouseShelfService {
    List<ProductDto> findAll();
    List<ProductDto> getMissingProducts(List<ProductDto> productsInOrder);
    void returnProductsToWarehouse(List<ProductInOrder> productInOrders);
    void removeProductsFromWarehouse(List<ProductDto> productInOrders);
    ProductDto updateQuantity(Long productId, int quantity);
    ProductDto insertProduct(Product product, int quantity);
}
