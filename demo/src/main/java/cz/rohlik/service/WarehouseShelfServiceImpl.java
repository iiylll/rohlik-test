package cz.rohlik.service;

import cz.rohlik.dao.WarehouseShelfRepository;
import cz.rohlik.dto.ProductDto;
import cz.rohlik.model.Product;
import cz.rohlik.model.ProductInOrder;
import cz.rohlik.model.WarehouseShelf;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class WarehouseShelfServiceImpl implements WarehouseShelfService {

    @Autowired
    WarehouseShelfRepository warehouseShelfRepository;

    @Override
    public List<ProductDto> findAll() {
        return warehouseShelfRepository.findAll().stream().map((shelf -> new ProductDto.ProductDtoBuilder()
                .product(shelf.getProduct())
                .quantity(shelf.getQuantity())
                .build()
        )).collect(Collectors.toList());
    }

    @Override
    public List<ProductDto> getMissingProducts(List<ProductDto> productDtos) {
        List<WarehouseShelf> warehouseShelves = findWarehouseShelves(productDtos);
        Map<Product, Integer> requestedProducts = productDtos.stream().collect(Collectors.toMap(ProductDto::copyProduct, ProductDto::getQuantity));
        return computeMissingQuantity(warehouseShelves, requestedProducts);
    }

    @Override
    public void returnProductsToWarehouse(List<ProductInOrder> productDtos) {
        productDtos.forEach((productDto -> {
            WarehouseShelf shelf = warehouseShelfRepository.findByProductId(productDto.getProduct().getId());
            shelf.addQuantity(productDto.getQuantity());
            warehouseShelfRepository.save(shelf);
        }));
    }

    @Override
    public void removeProductsFromWarehouse(List<ProductDto> productDtos) {
        productDtos.forEach((productDto -> {
            WarehouseShelf shelf = warehouseShelfRepository.findByProductId(productDto.getId());
            shelf.removeQuantity(productDto.getQuantity());
            warehouseShelfRepository.save(shelf);
        }));
    }

    @Override
    public ProductDto updateQuantity(Long productId, int quantity) {
        validateQuantity(quantity);
        WarehouseShelf shelf = warehouseShelfRepository.findByProductId(productId);
        shelf.setQuantity(quantity);
        warehouseShelfRepository.save(shelf);
        return new ProductDto.ProductDtoBuilder()
                .product(shelf.getProduct())
                .quantity(shelf.getQuantity())
                .build();
    }

    @Override
    public ProductDto insertProduct(Product product, int quantity) {
        WarehouseShelf shelf = new WarehouseShelf(product, quantity);
        if (warehouseShelfRepository.existsById(product.getId())) {
            throw new IllegalStateException();
        } else {
            shelf = warehouseShelfRepository.save(shelf);
            return new ProductDto.ProductDtoBuilder()
                    .product(shelf.getProduct())
                    .quantity(shelf.getQuantity())
                    .build();
        }
    }

    private List<ProductDto> computeMissingQuantity(List<WarehouseShelf> warehouseShelves, Map<Product, Integer> requestedProducts) {
        List<WarehouseShelf> shelvesWithMissingProducts = warehouseShelves.stream()
                .filter(x -> requestedProducts.get(x.getProduct()) > x.getQuantity())
                .collect(Collectors.toList());
        return shelvesWithMissingProducts.stream().map(shelf ->
                new ProductDto.ProductDtoBuilder()
                        .product(shelf.getProduct())
                        .quantity(requestedProducts.get(shelf.getProduct()) - shelf.getQuantity())
                        .build()).collect(Collectors.toList());
    }

    private List<WarehouseShelf> findWarehouseShelves(List<ProductDto> productsInOrder) {
        List<Long> productIds = productsInOrder.stream().map((ProductDto::getId)).collect(Collectors.toList());
        return warehouseShelfRepository.findAllByProductIdIn(productIds);
    }

    private void validateQuantity(int quantity) {
        if (quantity < 0) {
            throw new IllegalStateException();
        }
    }
}
