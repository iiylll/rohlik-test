package cz.rohlik.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.rohlik.dto.CreateProductRequest;
import cz.rohlik.dto.ProductDto;
import cz.rohlik.model.Product;
import cz.rohlik.service.ProductService;
import cz.rohlik.service.WarehouseShelfService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ContextConfiguration(classes = {ProductController.class})
@AutoConfigureMockMvc
@WebMvcTest
class ProductControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    ObjectMapper objectMapper;

    @MockBean
    private ProductService productService;

    @MockBean
    private WarehouseShelfService warehouseShelfService;

    private Product product;

    private final double PRICE = 5;
    private final String PRODUCT_NAME = "Jablko";
    private final int QUANTITY = 100;
    private final long PRODUCT_ID = 1;


    @BeforeEach
    void setUp() {
        product = new Product(PRODUCT_ID, PRODUCT_NAME, PRICE);
    }

    @Test
    void create() throws Exception {
        given(productService.create(PRODUCT_NAME, PRICE)).willReturn(product);
        given(warehouseShelfService.insertProduct(product, QUANTITY)).willReturn(
                new ProductDto.ProductDtoBuilder()
                        .id(PRODUCT_ID)
                        .name(PRODUCT_NAME)
                        .price(PRICE)
                        .quantity(QUANTITY)
                        .build()
        );
        CreateProductRequest request = new CreateProductRequest(PRODUCT_NAME, PRICE, QUANTITY);
        mvc.perform(post("/v1/product")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(request)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is(PRODUCT_NAME)))
                .andDo(MockMvcResultHandlers.print());
    }


    @Test
    void update() throws Exception {
        given(productService.update(PRODUCT_ID, PRODUCT_NAME, PRICE)).willReturn(product);
        given(warehouseShelfService.updateQuantity(product.getId(), QUANTITY)).willReturn(
                new ProductDto.ProductDtoBuilder()
                        .id(PRODUCT_ID)
                        .name(PRODUCT_NAME)
                        .price(PRICE)
                        .quantity(QUANTITY)
                        .build()
        );
                CreateProductRequest request = new CreateProductRequest(PRODUCT_NAME, PRICE, QUANTITY);
        mvc.perform(put("/v1/product/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(request)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is(PRODUCT_NAME)))
                .andDo(MockMvcResultHandlers.print());
    }
}