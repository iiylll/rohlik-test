package cz.rohlik.dao;

import cz.rohlik.model.Product;
import cz.rohlik.model.WarehouseShelf;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;


@ExtendWith(SpringExtension.class)
@SpringBootTest
public class WarehouseShelfRepositoryTest {
    @Autowired
    WarehouseShelfRepository warehouseShelfRepository;

    @Autowired
    ProductRepository productRepository;

    @Autowired
    OrderRepository orderRepository;

    private final double PRICE = 5;
    private final String PRODUCT_NAME = "Jablko";
    private final double NEW_PRICE = 7.7;
    private final String NEW_PRODUCT_NAME = "Mrkev";
    public static final int QUANTITY = 100;


    @Test
    void getAllByProductInPositive() {
        Product p = new Product(PRODUCT_NAME, PRICE);
        p = productRepository.save(p);

        WarehouseShelf warehouseShelf = new WarehouseShelf(p, QUANTITY);
        warehouseShelfRepository.save(warehouseShelf);

        Long id = productRepository.findAllByName(PRODUCT_NAME).get(0).getId();
        List<Long> ids = Lists.list(id);

        List<WarehouseShelf> ws = warehouseShelfRepository.findAllByProductIdIn(ids);
        assertNotEquals(ws.size(), 0);

    }

    @Test
    void getAllByProductInNegative() {
        List<Long> ids = Lists.list(Long.MAX_VALUE);
        List<WarehouseShelf> ws = warehouseShelfRepository.findAllByProductIdIn(ids);
        assertEquals(ws.size(), 0);

    }
}