package cz.rohlik.service;

import cz.rohlik.dao.OrderRepository;
import cz.rohlik.dao.ProductRepository;
import cz.rohlik.dao.WarehouseShelfRepository;
import cz.rohlik.dto.ProductDto;
import cz.rohlik.model.Order;
import cz.rohlik.model.Product;
import cz.rohlik.model.ProductInOrder;
import cz.rohlik.model.WarehouseShelf;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.persistence.EntityNotFoundException;
import java.util.List;

import static cz.rohlik.dao.WarehouseShelfRepositoryTest.QUANTITY;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class OrderServiceIntegrationTest {

    @Autowired
    OrderRepository orderRepository;

    @Autowired
    ProductRepository productRepository;

    @Autowired
    WarehouseShelfRepository warehouseShelfRepository;

    @Autowired
    OrderService orderService;

    private Product product;
    private Order order;
    private static final Order.OrderStatus ORDER_STATUS_PENDING = Order.OrderStatus.PENDING;
    private final double PRICE = 5;
    private final String PRODUCT_NAME = "Jablko";

    @BeforeEach
    public void setUp() {
        productRepository.deleteAll();
        warehouseShelfRepository.deleteAll();
        orderRepository.deleteAll();


        product = new Product(PRODUCT_NAME, PRICE);
        product = productRepository.save(product);

        WarehouseShelf warehouseShelf = new WarehouseShelf(product, QUANTITY);
        warehouseShelfRepository.save(warehouseShelf);

        ProductInOrder productInOrder = new ProductInOrder(product, QUANTITY);
        order = new Order(ORDER_STATUS_PENDING, Lists.list(productInOrder));
        productInOrder.setOrder(order);
        order = orderRepository.save(order);

    }


    @Test
    void createWithNotEnoughQuantityCheckAnswer() {
        List<ProductDto> missingProducts = orderService.create(createListOfOneProduct(product, 2 * QUANTITY));
        assertEquals(missingProducts.size(), 1);

    }

    @Test
    void createWithEnoughQuantityCheckAnswer() {
        List<ProductDto> missingProducts = orderService.create(createListOfOneProduct(product, QUANTITY));
        assertEquals(missingProducts.isEmpty(), true);
    }

    @Test
    void createWithNotEnoughQuantityCheckWarehouse() {
        orderService.create(createListOfOneProduct(product, 2 * QUANTITY));
        assertEquals(warehouseShelfRepository.findByProductId(product.getId()).getQuantity(), QUANTITY);
    }

    @Test
    void createWithEnoughQuantityCheckWarehouse() {
        orderService.create(createListOfOneProduct(product, QUANTITY));
        assertEquals(warehouseShelfRepository.findByProductId(product.getId()).getQuantity(), 0);
    }

    @Test
    void cancelOrderCheckWarehouseQuantities() {
        orderService.cancelOrder(order.getId());
        assertEquals(warehouseShelfRepository.findByProductId(product.getId()).getQuantity(), 2 * QUANTITY);
    }

    @Test
    void cancelOrderCheckStatusPositive() {
        orderService.cancelOrder(order.getId());
        assertEquals(orderRepository.findById(order.getId()).get().getOrderStatus(), Order.OrderStatus.CANCELED);
    }

    @Test
    void cancelOrderCheckStatusNegative() {
        assertThrows(EntityNotFoundException.class, () -> orderService.cancelOrder(Long.MAX_VALUE));
    }

    @Test
    void fulfillCheckStatusPositive() {
        orderService.fulfill(order.getId());
        assertEquals(orderRepository.findById(order.getId()).get().getOrderStatus(), Order.OrderStatus.PAYED);
    }

    @Test
    void fulfillCheckStatusNegative() {
        assertThrows(EntityNotFoundException.class, () -> orderService.fulfill(Long.MAX_VALUE));
    }

    private List<ProductDto> createListOfOneProduct(Product product, int quantity){
        return Lists.list(new ProductDto.ProductDtoBuilder().product(product).quantity(quantity).build());
    }
}