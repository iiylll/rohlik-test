package cz.rohlik.service;

import cz.rohlik.dao.ProductRepository;
import cz.rohlik.model.Product;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class ProductServiceIntegrationTest {

    @Autowired
    ProductService productService;

    @Autowired
    ProductRepository productRepository;

    private final double PRICE = 5;
    private final String PRODUCT_NAME = "Jablko";
    private final double NEW_PRICE = 7.7;
    private final String NEW_PRODUCT_NAME = "Mrkev";

    private Product existedProduct;

    @BeforeEach
    public void setUp() {
        productRepository.deleteAll();
        existedProduct = new Product(PRODUCT_NAME, PRICE);
        existedProduct = productRepository.save(existedProduct);
    }

    @Test
    void create() {
        productService.create(NEW_PRODUCT_NAME, PRICE);
        Product foundProduct = productRepository.findAllByName(PRODUCT_NAME).get(0);
        assertEquals(foundProduct.getPrice(), PRICE);
    }

    @Test
    void update() {
        productService.update(existedProduct.getId(), PRODUCT_NAME, NEW_PRICE);
        assertEquals(productRepository.findAllByName(existedProduct.getName()).get(0).getPrice(), NEW_PRICE);
    }

    @Test
    void delete() {
        assertEquals(productRepository.findAllByName(existedProduct.getName()).size(), 1);
        productService.delete(existedProduct.getId());
        assertEquals(productRepository.findAllByName(existedProduct.getName()).size(), 0);
    }
}