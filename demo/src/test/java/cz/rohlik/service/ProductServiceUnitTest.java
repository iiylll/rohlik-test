package cz.rohlik.service;

import cz.rohlik.dao.ProductRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(SpringExtension.class)
public class ProductServiceUnitTest {

    @InjectMocks
    ProductService productService = new ProductServiceImpl();

    @MockBean
    ProductRepository productRepository;

    private final long PRODUCT_ID = 1L;
    private final String PRODUCT_NAME = "Jablko";
    private final String ILLEGAL_PRODUCT_NAME = "";
    private final double PRICE = 50;
    private final double ILLEGAL_PRICE = -50.5;

    @Test
    public void createWithNegativePrice(){
        assertThrows(IllegalStateException.class, () -> productService.create(PRODUCT_NAME, ILLEGAL_PRICE));
    }

    @Test
    public void updateWithNegativePrice(){
        assertThrows(IllegalStateException.class, () -> productService.update(PRODUCT_ID, PRODUCT_NAME, ILLEGAL_PRICE));
    }

    @Test
    public void updateWithIllegalName(){
        assertThrows(IllegalStateException.class, () -> productService.update(PRODUCT_ID, ILLEGAL_PRODUCT_NAME, PRICE));
    }

    @Test
    public void createWithIllegalName(){
        assertThrows(IllegalStateException.class, () -> productService.create(ILLEGAL_PRODUCT_NAME, PRICE));
    }
}
