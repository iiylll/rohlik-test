package cz.rohlik.service;

import cz.rohlik.dao.ProductRepository;
import cz.rohlik.dao.WarehouseShelfRepository;
import cz.rohlik.model.Product;
import cz.rohlik.model.WarehouseShelf;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class WarehouseShelfServiceIntegrationTest {

    @Autowired
    WarehouseShelfRepository warehouseShelfRepository;

    @Autowired WarehouseShelfService warehouseShelfService;

    @Autowired
    ProductRepository productRepository;

    Product product;

    private final double PRICE = 5;
    private final String PRODUCT_NAME = "Jablko";
    private final int QUANTITY = 100;
    private final int NEW_QUANTITY = 200;

    @BeforeEach
    public void setUp() {
        warehouseShelfRepository.deleteAll();
        productRepository.deleteAll();
        product = new Product(PRODUCT_NAME, PRICE);
        product = productRepository.save(product);
    }


    @Test
    void updateQuantity() {
        WarehouseShelf shelf = new WarehouseShelf(product, QUANTITY);
        warehouseShelfRepository.save(shelf);
        warehouseShelfService.updateQuantity(product.getId(), NEW_QUANTITY);
        assertEquals(warehouseShelfRepository.findByProductId(product.getId()).getQuantity(), NEW_QUANTITY);
    }

    @Test
    void insertProduct() {
        warehouseShelfService.insertProduct(product, QUANTITY);
        assertEquals(warehouseShelfRepository.findByProductId(product.getId()).getQuantity(), QUANTITY);
    }
}
