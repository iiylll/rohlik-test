package cz.rohlik.service;

import cz.rohlik.dao.WarehouseShelfRepository;
import cz.rohlik.dto.ProductDto;
import cz.rohlik.model.Product;
import cz.rohlik.model.WarehouseShelf;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(SpringExtension.class)
class WarehouseShelfServiceUnitTest {

    @Mock
    private WarehouseShelfRepository warehouseShelfRepository;

    @InjectMocks
    private WarehouseShelfService warehouseShelfService = new WarehouseShelfServiceImpl();

    private Product productOne;
    private Product productTwo;
    public final static int ILLEGAL_QUANTITY = -5;
    public final static int QUANTITY_IN_ORDER = 30;
    public final static int QUANTITY_IN_WAREHOUSE = 30;
    public final static int QUANTITY_IN_HUGE_WAREHOUSE = 50;
    public final static int QUANTITY_IN_WAREHOUSE_BUT_NOT_ENOUGH = 10;


    @BeforeEach
    public void setUp() {

        productOne = new Product(1L);
        productTwo = new Product(2L);
        List<WarehouseShelf> warehouseShelfList = Lists.list(new WarehouseShelf(productOne, QUANTITY_IN_ORDER), new WarehouseShelf(productTwo, QUANTITY_IN_ORDER));
        Mockito.when(warehouseShelfRepository.findAllByProductIdIn(Mockito.anyList()))
                .thenReturn(warehouseShelfList);

    }

    @Test
    void updateQuantityValidationTest() {
        assertThrows(IllegalStateException.class, () -> warehouseShelfService.updateQuantity(productOne.getId(), ILLEGAL_QUANTITY));
    }

    @Test
    void getMissingProductsCheckListSizeWhenQuantityIsEnough() {
        List<ProductDto> missingProducts = warehouseShelfService.getMissingProducts(createListOfTwoProduct(productOne, QUANTITY_IN_HUGE_WAREHOUSE, productTwo, QUANTITY_IN_HUGE_WAREHOUSE));
        assertEquals(missingProducts.size(), 2);
    }

    @Test
    void getMissingProductsCheckListSizeWhenQuantityIsNotEnough() {
        List<ProductDto> missingProducts = warehouseShelfService.getMissingProducts(createListOfTwoProduct(productOne, QUANTITY_IN_WAREHOUSE_BUT_NOT_ENOUGH, productTwo, QUANTITY_IN_WAREHOUSE_BUT_NOT_ENOUGH));
        assertEquals(missingProducts.size(), 0);
    }

    @Test
    void getMissingProductsCheckListSize() {
        List<ProductDto> missingProducts = warehouseShelfService.getMissingProducts(createListOfTwoProduct(productOne, QUANTITY_IN_WAREHOUSE, productTwo, QUANTITY_IN_WAREHOUSE));
        assertEquals(missingProducts.size(), 0);
    }

    private List<ProductDto> createListOfTwoProduct(Product product, int firstQuantity, Product secondProduct, int secondQuantity) {
        return Lists.list(
                new ProductDto.ProductDtoBuilder().product(product).quantity(firstQuantity).build(),
                new ProductDto.ProductDtoBuilder().product(secondProduct).quantity(secondQuantity).build()
        );
    }
}